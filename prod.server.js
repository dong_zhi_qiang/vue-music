var express = require('express')

var axios = require('axios')

var port = '3100'

var app = express()

var apiRoutes = express.Router()


// 由于请求的referer和host不同，所以前端不能拿到数据，需要后端做一个代理
      //  后端向有数据的服务端发送请求，拿到数据，然后前端在向自己的服务器请求那数据
      //  这里使用axios实现ajax请求：axios是一个基于promise的HTTP库，可以用于浏览器和node.js
      // 在浏览器创建XMLHttpRequest对象，从node.js创建http请求
      app.get('/api/getDiscList', (req, res) => {//这里的路径是给前端发送请求的url
        var url = 'https://c.y.qq.com/splcloud/fcgi-bin/fcg_get_diss_by_tag.fcg'
        // axios发送get请求，可以自己配置config
        axios.get(url, {
          headers: {  //通过node请求QQ接口，发送http请求时，修改referer和host
            referer: 'https://c.y.qq.com/',
            host: 'c.y.qq.com'
          },
           //  params是即将与请求一起发送的url参数，无格式对象/URLSearchParams对象
          params: req.query  //把前端传过来的params，全部给QQ的url
        }).then((response) => {  //成功回调
          res.json(response.data)  //response是QQ接口返回的，res是我们自己的。所以要把数据输出给浏览器前端
        }).catch((e) => {
          console.log(e)
        })
      }),
      app.get('/api/music', function (req, res) {//这里的路径是给前端发送请求的url
        const url = 'https://c.y.qq.com/base/fcgi-bin/fcg_music_express_mobile3.fcg'
        // axios发送get请求，可以自己配置config
        axios.get(url, {
          headers: { ////通过node请求QQ接口，发送http请求时，修改referer和host
            referer: 'https://y.qq.com/',
            host: 'c.y.qq.com'
          },
          //  params是即将与请求一起发送的url参数，无格式对象/URLSearchParams对象
          params: req.query ////把前端传过来的params，全部给QQ的url
        }).then((response) => {
          res.json(response.data)
        }).catch((e) => {
          console.log(e)
        })
      }),
      app.get('/api/lyric', function(req, res){// 获取音乐歌词
        var url="https://szc.y.qq.com/lyric/fcgi-bin/fcg_query_lyric_new.fcg"

        axios.get(url, {
          headers: {  //通过node请求QQ接口，发送http请求时，修改referer和host
            referer: 'https://y.qq.com/',
            host: 'c.y.qq.com'
          },
          params: req.query //把前端传过来的params，全部给QQ的url
          }).then((response) => {
              //  res.json(response.data)
              //将QQ返回的jsonp文件转换为json格式
               var ret = response.data
               if (typeof ret === 'string') {
                   var reg = /^\w+\(({[^()]+})\)$/
                   // 以单词a-z，A-Z开头，一个或多个
                   // \(\)转义括号以（）开头结尾
                   // （）是用来分组
                   // 【^()】不以左括号/右括号的字符+多个
                   // {}大括号也要匹配到
                   var matches = ret.match(reg)
                   if (matches) {
                       ret = JSON.parse(matches[1])
                       // 对匹配到的分组的内容进行转换
                   }
              }
              res.json(ret)
          }).catch((e) => {
               console.log(e)
        })
      }),
      app.get('/api/getSongList', function (req, res) { //获取音乐列表
        var url = 'https://c.y.qq.com/qzone/fcg-bin/fcg_ucc_getcdinfo_byids_cp.fcg'
        axios.get(url, {
          headers: {
            referer: 'https://y.qq.com/',
            host: 'c.y.qq.com'
          },
          params: req.query
        }).then((response) => {
          res.json(response.data)
        }).catch((e) => {
          console.log(e)
        })
      }),
      app.get('/api/getSearch', function (req, res) {//搜索结果数据
        var url = 'https://c.y.qq.com/soso/fcgi-bin/search_for_qq_cp'
        axios.get(url, {
          headers: {
            referer: 'https://c.y.qq.com/',
            host: 'c.y.qq.com'
          },
          params: req.query
        }).then((response) => {
          res.json(response.data)
        }).catch((e) => {
          console.log(e)
        })
      })



app.use('/api', apiRoutes)

app.use(express.static('./dist'))

module.exports = app.listen(port, function (err) {
  if (err) {
    console.log(err)
    return
  }
  console.log('Listening at http://localhost:' + port + '\n')
})
