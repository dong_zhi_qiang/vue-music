import jsonp from 'common/js/jsonp'
import {commonParams, options} from './config'
import axios from 'axios'

/**
 * data: 固定参数，不知道含义，但一定要与接口保持一致
 */

export function getRecommend() {//轮播图请求接口参数
  const url = 'https://c.y.qq.com/musichall/fcgi-bin/fcg_yqqhomepagerecommend.fcg'

  const data = Object.assign({}, commonParams, {
    platform: 'h5',
    uin: 0,
    needNewCode: 1
  })
  return jsonp(url, data, options)
}

export function getDiscList() {
  const url = '/api/getDiscList'  //调用自定义的接口

  const data = Object.assign({}, commonParams, {
    picmid: 1,
    loginUin: 0,
    hostUin: 0,
    platform: 'yqq.json',
    needNewCode: 0,
    categoryId: 10000000,
    rnd: Math.random(),
    sortId: 5,
    sin: 0,
    ein: 19,
    format: 'json'  //使用的时axios,所以format使用的是json,不是jsonp
  })
  return axios.get(url, {
    params: data
  }).then((res) => {
    return Promise.resolve(res.data)  //es6新语法，返回一个以给定值解析后的Promise对象
  })
}


export function getSongList(disstid) { //获取歌曲列表
  const url = '/api/getSongList'

  const data = Object.assign({}, commonParams, {
    uin: 0,
    format: 'json',
    notice: 0,
    needNewCode: 1,
    new_format: 1,
    pic: 500,
    disstid, //关键数据
    type: 1,
    json: 1,
    utf8: 1,
    onlysong: 0,
    picmid: 1,
    nosign: 1,
    song_begin: 0,
    platform: 'h5',
    song_num: 100,
    _: +new Date()
  })

  return axios.get(url, {
    params: data
  }).then((res) => {
    return Promise.resolve(res.data)
  })
}