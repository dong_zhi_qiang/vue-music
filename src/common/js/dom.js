/**
 * 封装一些DOM操作相关的代码
 */

export function hasClass(el, className) { //判断有没有class 属性
  let reg = new RegExp('(^|\\s)' + className + '(\\s|$)')
  return reg.test(el.className)
}

export function addClass(el, classNames) {
  if (hasClass(el, classNames)) {
    return
  }
  let newClass = el.className.split(' ') //对class属性 进行拆封成数组
  newClass.push(classNames) //添加class属性
  el.className = newClass.join(' ')  //合并class属性

}

export function getData(el, name, val) {
  const prefix = 'data-'
  if (val) {
    return el.setAttribute(prefix + name, val)
  }
  return el.getAttribute(prefix + name)
}

//能力检测: 查看elementStyle支持哪些特性
let elementStyle = document.createElement('div').style

//供应商: 遍历查找浏览器的前缀名称，返回对应的当前浏览器
let vendor = (() => {
  let transformNames = {
    webkit: 'webkitTransform',
    Moz: 'MozTransform',
    O: 'OTransform',
    ms: 'msTransform',
    standard: 'transform'
  }

  for (let key in transformNames) {
    if (elementStyle[transformNames[key]] !== undefined) {
      return key
    }
  }

  return false
})()

export function prefixStyle(style) {
  if (vendor === false) {
    return false
  }

  if (vendor === 'standard') {
    return style
  }

  return vendor + style.charAt(0).toUpperCase() + style.substr(1)
}
