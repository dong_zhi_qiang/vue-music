/**
 * 初始化Vuex入口文件
 */


import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import state from './state'
import mutations from './mutations'
import createLogger from 'vuex/dist/logger'

Vue.use(Vuex)//注册插件

const debug = process.env.NODE_ENV !== 'production'  //调试工具


export default new Vuex.Store({  //类似单列模式
  actions,
  getters,
  state,
  mutations,
  // strict: debug,
  plugins: debug ? [createLogger()] : []
})
