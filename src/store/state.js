/**
 * 管理所有状态 state
 */
import {playMode} from 'common/js/config'
import {loadSearch, loadPlay, loadFavorite} from 'common/js/cache'

const state = {
  singer: {},
  playing: false, //播放状态  默认暂停
  fullScreen: false, //播放界面状态 全屏 默认关闭
  playlist: [],//播放列表
  sequenceList: [], //循序列表
  mode: playMode.sequence,
  currentIndex: -1, //当前播放索引
  disc: {}, //歌单对象
  topList: {}, //排行榜列表书籍
  searchHistory: loadSearch(),// 搜索历史
  playHistory: loadPlay(),//播放历史
  favoriteList: loadFavorite() //我的收藏
}


export default state